# Game 'Snake'
**author**: Drakoriss (Viktor Sobolivskyi) anmunimei@gmail.com

**date**: 2019-03-27

## Description
In a field NxM player controls a snake of small size. Sometimes randomly spawns pick-ups that snake can eat just by moving onto it - if so, snake increase it's size. Snake always moves forward and player can control only turns (left or right). Snake dies if it hits field walls or itself.

## Rules

* world
 * has fixed rectangular size WxH
 * contains PC (playable character) in a form of a snake
 * contains pick-ups in a form of a food
 * contains impassable walls

* PC
 * player possesses (means that player can control PC) PC when game state changes to 'playing'
 * has size 1xN where N = 3 initially
 * always moves forward in direction of snake's head
 * can only be turned to the left or to the right
 * movement speed constantly increases upon picking up food
 * when collides with food increase it's length (N)
 * when collides with itself or any wall stops the movement and releases (means that player cannot control PC) from player

* control
 * arrows and WASD turns snake by 90 degree at state 'playing'
 * if PC moves up or down then only Left/Right/A/D can be used for turn
 * if PC moves left or right then only Up/Down/W/S can be used for turn
 * Esc will exits the game at any moment
 * Enter can be used to start a game at state 'menu'

* state
 * menu: game ready to start, occurs when game loaded or after gameover
 * playing: game process started, player can control PC

* UI (user interface)
 * all info displayed at the top of the screen above game field
 * 'length: L' displayed on the left side and represents a length of the PC
 * 'score: S' displayed in a middle and represents how successful a player is
 * 'speed: V' displayed on the right side and represents PC's speed

## Mechanics
PC represented on game field by array of positions (X=[1, W-1], Y=[1, H-1]) of head, body and tail; direction (X=[-1, 1], Y=[-1, 1]) where head will be moved; and speed (V=[0.1, 1.0]) in seconds which used as interval between head and tail reposition.
Every V seconds tail from array removed and new head pushed with position oldTail.xy + directon.xy.

When game starts one portion of food spawned randomly in [1, W-1 : 1, H-1] range excluding PC head, body and tail. When head overlaps food tail are not removed one time (length grow effect).

Borders of game field are covered by walls. When head would overlap any wall or itself body/tail movement stops and game moves to state 'menu'.

## Sound
When game loads looped music track should start. PC moving, food consumption, wall/self hitting should have short sound effects.
