push = require 'lib/push'

function love.load()
  windowWidth, windowHeight = 1280, 720
  virtualWidth, virtualHeight = 360, 360

  bgColor = {0.105, 0.109, 0.086, 1}
  textColor = {0.482, 0.498, 0.392, 1}
  bodyColor = {0.458, 0.498, 0.258, 1}
  headColor = {0.913, 1, 0.486, 1}
  foodColor = {0.964, 1, 0.784, 1}

  smallFont = love.graphics.newFont('assets/FreeSans.ttf', 16)
  largeFont = love.graphics.newFont('assets/FreeSans.ttf', 32)

  sounds = {
    ['move'] = love.audio.newSource('assets/move.wav', 'static'),
    ['food'] = love.audio.newSource('assets/food.wav', 'static'),
    ['hit'] = love.audio.newSource('assets/hit.wav', 'static'),
    ['music'] = love.audio.newSource('assets/Underclocked.mp3', 'static')
  }
  sounds['music']:setLooping(true)
  sounds['music']:play()

  love.graphics.setDefaultFilter('nearest', 'nearest')
  love.window.setTitle('Snake')

  push:setupScreen(virtualWidth, virtualHeight, windowWidth, windowHeight, {
    fullscreen = false,
    resizable = true,
    vsync = true
  })

  math.randomseed(os.time())

  snake = {{30, 50}, {30, 60}, {30, 70}}
  direction = {0, 10}
  food = {70, 240}
  speeds = {
    0.5,
    0.4,
    0.3,
    0.2,
    0.1,
    0.08,
    0.06,
    0.04,
    0.02,
    0.01
  }

  score = 0
  scoreText = love.graphics.newText(smallFont)
  speed = speeds[1]
  speedText = love.graphics.newText(smallFont)

  enterText = love.graphics.newText(largeFont, "press 'Enter' to start")
  escText = love.graphics.newText(largeFont, "or 'Esc' to quit")

  state = 'menu'
  interval = 0
end

function love.resize(w, h)
  push:resize(w, h)
end

function love.update(dt)
  if state == 'playing' then
    interval = interval + dt

    if interval >= speed then
      interval = 0
      local head = snake[#snake]
      table.insert(snake, {head[1] + direction[1], head[2] + direction[2]})

      head = snake[#snake]
      for k, v in pairs(snake) do
        if k ~= #snake then
          if head[1] == v[1] and head[2] == v[2] then
            state = 'menu'
            table.remove(snake, 1)
            sounds['hit']:play()
            return
          end
        end
      end

      if head[1] < 0 or head[1] > virtualWidth then
        state = 'menu'
        table.remove(snake, 1)
        sounds['hit']:play()
      elseif head[2] < 20 or head[2] > virtualHeight then
        state = 'menu'
        table.remove(snake, 1)
        sounds['hit']:play()
      else
        if head[1] == food[1] and head[2] == food[2] then
          spawnFood()
          addScore()
          if #snake % 3 == 0 then
            increaseSpeed()
          end
          sounds['food']:play()
        else
          table.remove(snake, 1)
          sounds['move']:play()
        end
      end
    end
  end
end

function love.draw()
  push:apply('start')

  love.graphics.clear()

  love.graphics.setColor(bgColor)
  love.graphics.rectangle('fill', 0, 20, virtualWidth, virtualHeight)

  love.graphics.setColor(foodColor)
  love.graphics.rectangle('fill', food[1], food[2], 10, 10)

  love.graphics.setColor(bodyColor)
  for k, v in pairs(snake) do
    if k == #snake then
      love.graphics.setColor(headColor)
    end
    love.graphics.rectangle('fill', v[1], v[2], 10, 10)
  end

  renderUI()

  if state == 'menu' then
    love.graphics.setColor(textColor)
    local enterW = enterText:getWidth() / 2
    local escW = escText:getWidth() / 2
    love.graphics.draw(enterText, virtualWidth / 2 - enterW, virtualHeight / 2 - 20)
    love.graphics.draw(escText, virtualWidth / 2 - escW, virtualHeight / 2 + 20)
  end

  push:apply('end')
end

function love.keypressed(key)
  if key == 'escape' then
    love.event.quit()
  elseif key == 'enter' or key == 'return' then
    if state == 'menu' then
      interval = 0
      speed = speeds[1]
      score = 0
      snake = {{30, 50}, {30, 60}, {30, 70}}
      direction = {0, 10}
      spawnFood()
      state = 'playing'
    end
  elseif key == 'up' or key == 'w' then
    if state == 'playing' and direction[2] == 0 then
      direction = {0, -10}
    end
  elseif key == 'down' or key == 's' then
    if state == 'playing' and direction[2] == 0 then
      direction = {0, 10}
    end
  elseif key == 'left' or key == 'a' then
    if state == 'playing' and direction[1] == 0 then
      direction = {-10, 0}
    end
  elseif key == 'right' or key == 'd' then
    if state == 'playing' and direction[1] == 0 then
      direction = {10, 0}
    end
  end
end

function spawnFood()
  local notFound = true
  local newFood = {}

  while notFound do
    if speed > 0.1 then
      local x = snake[#snake][1] / 10
      local y = snake[#snake][2] / 10
      newFood = {
        math.random(math.max(0, x - 10), math.min(virtualWidth / 10 - 1, x + 10)),
        math.random(math.max(20, y - 10), math.min(virtualHeight / 10 - 1, y + 10))
      }
    else
      newFood = {math.random(0, virtualWidth / 10 - 1), math.random(20, virtualHeight / 10 - 1)}
    end
    notFound = false

    for _, v in pairs(snake) do

      if newFood[1] == v[1] / 10 and newFood[2] == v[2] / 10 then
        notFound = true
        break
      end
    end
  end

  food = {newFood[1] * 10, newFood[2] * 10}
end

function increaseSpeed()
  if speed ~= speeds[#speeds] then
    for k, v in pairs(speeds) do
      if v == speed then
        speed = speeds[k + 1]
        break
      end
    end
  end
end

function addScore()
  if score == 0 then
    score = 1
  else
    score = score + 8 / speed
  end
end

function renderUI()
  love.graphics.setFont(smallFont)
  love.graphics.setColor(textColor)
  love.graphics.print('length: ' .. tostring(#snake), 10, 0)

  scoreText:set('score: ' .. tostring(math.floor(score)))
  local scoreW = scoreText:getWidth() / 2
  love.graphics.draw(scoreText, virtualWidth / 2 - scoreW, 0)

  local value = 1
  for k, v in pairs(speeds) do
    if v == speed then
      value = k
      break
    end
  end
  speedText:set('speed: ' .. tostring(value))
  local speedW = speedText:getWidth()
  love.graphics.draw(speedText, virtualWidth - speedW - 10, 0)
end

function displayFps()
  love.graphics.setFont(smallFont)
  love.graphics.setColor(textColor)
  love.graphics.print('FPS: ' .. tostring(love.timer.getFPS()), 10, 10)
end
